package com.sculptor.law;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SearchView;


public class MainActivity2 extends AppCompatActivity implements SearchView.OnQueryTextListener{
    ListView lv;
    SearchView sv;
    ArrayAdapter<String> adapter;
    String[] data={"সরকারী কর্মচারীকে ক্ষতিসাধনের হুমকি","সরকারী কর্মচারী দ্বারা জারিকৃত আদেশ অমান্য করা","ধর্মীয় সমাবেশে গোলযোগ সৃষ্টি করা",
            "ধর্মীয় অনুভূতিতে আঘাত হানার চেষ্টা করা","খুন করা","শাস্তিযোগ্য নরহত্যা","ইচ্ছাকৃতভাবে আঘাত করা","মারাত্মক অস্ত্র দ্বারা ইচ্ছাকৃতভাবে আঘাত করা",
            "শক্তি প্রয়োগ করে সম্পতি ছিনিয়ে নেয়া","বিষ প্রয়োগের মাধ্যমে আহত করা","এসিড প্রয়োগ করে মুখমণ্ডল ও চোখ বিকৃত করা","অপরাধমূলক ষড়যন্ত্র",
            "মুদ্রা জালকরন","মুদ্রা জালকরন করার যন্ত্র প্রস্তুত ও বিক্রয়","মুদ্রা জালকরন করার জন্য যন্ত্র দখলে রাখা","জাল মুদ্রা আমদানি ও রপ্তানি",
            "মুদ্রা জাল বলে জানা সত্ত্বেও উহা হস্তান্তর","সরকারী স্ট্যাম্প জালকরন","সরকারী স্ট্যাম্প জালকরন করার জন্য যন্ত্রপাতি দখলে রাখা",
            "জাল সরকারী স্ট্যাম্প বিক্রয় করা","জাল সরকারী স্ট্যাম্প দখলে রাখা","মানহানি","মানহানিকর বস্তু বিক্রয় করা","চোরাই সম্পতি অসাধুভাবে গ্রহন",
            "ক্ষতিকর খাদ্য ও পানীয় বিক্রয়","ওষুধ ও চিকিৎসা দ্রব্যে ভেজাল মিশানো ","ভেজাল মিশ্রিত ওষুধ বিক্রয়","জলাশয়ের পানি দূষিত করা",
            "আবহাওয়াকে স্বাস্থ্যের পক্ষে ক্ষতিকর করে তোলা","সড়কে বেপরোয়াভাবে গাড়ি চালানো","বেপরোয়াভাবে নৌযান চালানো",
            "ভাড়ার জন্য নৌযানে নিরাপত্তাহীন ভাবে যাত্রী বহন করা","আত্মহত্যা করার চেষ্টা করা","আত্মহত্যায় প্ররোচনা দান করা","খুনের উদ্যোগ গ্রহন",
            "অবহেলার ফলে মৃত্যু সংঘটন","স্ত্রী বা শিশুর সাথে সামাজিক নিয়মের বিরুদ্ধে যৌন সঙ্গম","চুরি করার সাজা","বাসগৃহে চুরি করার সাজা",
            "চুরির উদ্দেশে মৃত্যু ঘটানো, আঘাত করা বা আটকানোর প্রস্তুতি গ্রহন","দস্যুতার সাজা","ডাকাতির সাজা","খুন সহকারে ডাকাতি","ডাকাত দলে থাকার সাজা",
            "চোরের দলে থাকার সাজা","দস্যুতা ঘটানোর সময় ইচ্ছাকৃত ভাবে আঘাত করা","প্রতারনার সাজা","অপরের রুপ ধারন করে প্রতারনার সাজা",
            "স্বামী বা স্ত্রী বেঁচে থাকা সত্ত্বেও পুনরায় বিবাহ করা","অপরের স্ত্রীর সাথে যৌন সঙ্গম","স্ত্রীলোকের সম্মতি ছাড়া গর্ভপাত করানো",
            "জীবন্ত জন্মজাত শিশুর মৃত্যু ঘটানো","পিতামাতা দ্বারা শিশুকে বর্জন করা","মৃতদেহ গোপনে অপসারণ করে জন্ম গোপন করা","অবৈধ বাধাদান করার সাজা",
            "অবৈধ অবরোধ এর সাজা","গোপন স্থানে অবৈধভাবে বন্দি রাখা","বল প্রয়োগ করে সম্পতি ছিনিয়ে নেয়া","অপহরন","বেআইনি সমাবেশে অংশগ্রহন করা",
            "দাঙ্গা করার সাজা","দাঙ্গা করার সময় সরকারী কর্মচারীকে আঘাত করা","বেআইনি সমাবেশে অংশগ্রহণ করা","মারামারি করার সাজা","মিথ্যা সাক্ষ্যদানের সাজা",
            "মিথ্যা সার্টিফিকেট ইস্যু করা","দোষী লোককে আশ্রয় প্রদান","কারো আইনসঙ্গত গ্রেফতারে বাধা দান",
            "মিথ্যা বাটখারা বা মাপ তৈরি কিংবা বিক্রয় করা","অগ্নি বা দাহ্য বস্তু নিয়ে অবহেলামূলক আচরন করা",
            "প্রাণী নিয়ে অবহেলামূলক আচরন করা","নিষেধাজ্ঞার পরেও উৎপাত অব্যাহত রাখা","অশ্লীল পুস্তকাদি বিক্রয়","সড়ক আইন"};


    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setLogo(R.drawable.ic_launcher);
        getSupportActionBar().setDisplayUseLogoEnabled(true);
        setContentView(R.layout.activity_main2);
        lv=(ListView)findViewById(R.id.idlistview);
        sv=(SearchView)findViewById(R.id.idsearch);
        adapter=new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,data);
        lv.setAdapter(adapter);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String data=(String) adapterView.getItemAtPosition(i);
                if(i==0)
                {
                    Intent s=new Intent(view.getContext(),Dhara1.class);
                    startActivity(s);
                }
                if(i==1)
                {
                    Intent s=new Intent(view.getContext(),Dhara2.class);
                    startActivity(s);
                }
                if(i==2)
                {
                    Intent s=new Intent(view.getContext(),Dhara3.class);
                    startActivity(s);
                }
                if(i==3)
                {
                    Intent s=new Intent(view.getContext(),Dhara4.class);
                    startActivity(s);
                }
                if(i==4)
                {
                    Intent s=new Intent(view.getContext(),Dhara5.class);
                    startActivity(s);
                }
                if(i==5)
                {
                    Intent s=new Intent(view.getContext(),Dhara6.class);
                    startActivity(s);
                }
                if(i==6)
                {
                    Intent s=new Intent(view.getContext(),Dhara7.class);
                    startActivity(s);
                }
                if(i==7)
                {
                    Intent s=new Intent(view.getContext(),Dhara8.class);
                    startActivity(s);
                }
                if(i==8)
                {
                    Intent s=new Intent(view.getContext(),Dhara9.class);
                    startActivity(s);
                }
                if(i==9)
                {
                    Intent s=new Intent(view.getContext(),Dhara10.class);
                    startActivity(s);
                }
                if(i==10)
                {
                    Intent s=new Intent(view.getContext(),Dhara11.class);
                    startActivity(s);
                }
                if(i==11)
                {
                    Intent s=new Intent(view.getContext(),Dhara12.class);
                    startActivity(s);
                }
                if(i==12)
                {
                    Intent s=new Intent(view.getContext(),Dhara13.class);
                    startActivity(s);
                }
                if(i==13)
                {
                    Intent s=new Intent(view.getContext(),Dhara14.class);
                    startActivity(s);
                }
                if(i==14)
                {
                    Intent s=new Intent(view.getContext(),Dhara15.class);
                    startActivity(s);
                }
                if(i==15)
                {
                    Intent s=new Intent(view.getContext(),Dhara16.class);
                    startActivity(s);
                }
                if(i==16)
                {
                    Intent s=new Intent(view.getContext(),Dhara17.class);
                    startActivity(s);
                }
                if(i==17)
                {
                    Intent s=new Intent(view.getContext(),Dhara18.class);
                    startActivity(s);
                }
                if(i==18)
                {
                    Intent s=new Intent(view.getContext(),Dhara19.class);
                    startActivity(s);
                }
                if(i==19)
                {
                    Intent s=new Intent(view.getContext(),Dhara20.class);
                    startActivity(s);
                }
                if(i==20)
                {
                    Intent s=new Intent(view.getContext(),Dhara21.class);
                    startActivity(s);
                }
                if(i==21)
                {
                    Intent s=new Intent(view.getContext(),Dhara22.class);
                    startActivity(s);
                }
                if(i==22)
                {
                    Intent s=new Intent(view.getContext(),Dhara23.class);
                    startActivity(s);
                }
                if(i==23)
                {
                    Intent s=new Intent(view.getContext(),Dhara24.class);
                    startActivity(s);
                }
                if(i==24)
                {
                    Intent s=new Intent(view.getContext(),Dhara25.class);
                    startActivity(s);
                }
                if(i==25)
                {
                    Intent s=new Intent(view.getContext(),Dhara26.class);
                    startActivity(s);
                }
                if(i==26)
                {
                    Intent s=new Intent(view.getContext(),Dhara27.class);
                    startActivity(s);
                }
                if(i==27)
                {
                    Intent s=new Intent(view.getContext(),Dhara28.class);
                    startActivity(s);
                }
                if(i==28)
                {
                    Intent s=new Intent(view.getContext(),Dhara29.class);
                    startActivity(s);
                }
                if(i==29)
                {
                    Intent s=new Intent(view.getContext(),Dhara30.class);
                    startActivity(s);
                }
                if(i==30)
                {
                    Intent s=new Intent(view.getContext(),Dhara31.class);
                    startActivity(s);
                }
                if(i==31)
                {
                    Intent s=new Intent(view.getContext(),Dhara32.class);
                    startActivity(s);
                }
                if(i==32)
                {
                    Intent s=new Intent(view.getContext(),Dhara33.class);
                    startActivity(s);
                }
                if(i==33)
                {
                    Intent s=new Intent(view.getContext(),Dhara35.class);
                    startActivity(s);
                }
                if(i==34)
                {
                    Intent s=new Intent(view.getContext(),Dhara36.class);
                    startActivity(s);
                }
                if(i==35)
                {
                    Intent s=new Intent(view.getContext(),Dhara37.class);
                    startActivity(s);
                }
                if(i==36)
                {
                    Intent s=new Intent(view.getContext(),Dhara38.class);
                    startActivity(s);
                }
                if(i==37)
                {
                    Intent s=new Intent(view.getContext(),Dhara39.class);
                    startActivity(s);
                }
                if(i==38)
                {
                    Intent s=new Intent(view.getContext(),Dhara40.class);
                    startActivity(s);
                }
                if(i==39)
                {
                    Intent s=new Intent(view.getContext(),Dhara41.class);
                    startActivity(s);
                }
                if(i==40)
                {
                    Intent s=new Intent(view.getContext(),Dhara42.class);
                    startActivity(s);
                }
                if(i==41)
                {
                    Intent s=new Intent(view.getContext(),Dhara43.class);
                    startActivity(s);
                }
                if(i==42)
                {
                    Intent s=new Intent(view.getContext(),Dhara44.class);
                    startActivity(s);
                }
                if(i==43)
                {
                    Intent s=new Intent(view.getContext(),Dhara45.class);
                    startActivity(s);
                }
                if(i==44)
                {
                    Intent s=new Intent(view.getContext(),Dhara46.class);
                    startActivity(s);
                }
                if(i==45)
                {
                    Intent s=new Intent(view.getContext(),Dhara47.class);
                    startActivity(s);
                }
                if(i==46)
                {
                    Intent s=new Intent(view.getContext(),Dhara48.class);
                    startActivity(s);
                }
                if(i==47)
                {
                    Intent s=new Intent(view.getContext(),Dhara49.class);
                    startActivity(s);
                }
                if(i==48)
                {
                    Intent s=new Intent(view.getContext(),Dhara50.class);
                    startActivity(s);
                }
                if(i==49)
                {
                    Intent s=new Intent(view.getContext(),Dhara51.class);
                    startActivity(s);
                }
                if(i==50)
                {
                    Intent s=new Intent(view.getContext(),Dhara52.class);
                    startActivity(s);
                }
                if(i==51)
                {
                    Intent s=new Intent(view.getContext(),Dhara53.class);
                    startActivity(s);
                }
                if(i==52)
                {
                    Intent s=new Intent(view.getContext(),Dhara54.class);
                    startActivity(s);
                }
                if(i==53)
                {
                    Intent s=new Intent(view.getContext(),Dhara55.class);
                    startActivity(s);
                }
                if(i==54)
                {
                    Intent s=new Intent(view.getContext(),Dhara56.class);
                    startActivity(s);
                }
                if(i==54)
                {
                    Intent s=new Intent(view.getContext(),Dhara56.class);
                    startActivity(s);
                }
                if(i==55)
                {
                    Intent s=new Intent(view.getContext(),Dhara57.class);
                    startActivity(s);
                }
                if(i==56)
                {
                    Intent s=new Intent(view.getContext(),Dhara58.class);
                    startActivity(s);
                }
                if(i==57)
                {
                    Intent s=new Intent(view.getContext(),Dhara59.class);
                    startActivity(s);
                }
                if(i==58)
                {
                    Intent s=new Intent(view.getContext(),Dhara60.class);
                    startActivity(s);
                }
                if(i==59)
                {
                    Intent s=new Intent(view.getContext(),Dhara61.class);
                    startActivity(s);
                }
                if(i==60)
                {
                    Intent s=new Intent(view.getContext(),Dhara62.class);
                    startActivity(s);
                }
                if(i==61)
                {
                    Intent s=new Intent(view.getContext(),Dhara63.class);
                    startActivity(s);
                }
                if(i==62)
                {
                    Intent s=new Intent(view.getContext(),Dhara64.class);
                    startActivity(s);
                }
                if(i==63)
                {
                    Intent s=new Intent(view.getContext(),Dhara65.class);
                    startActivity(s);
                }
                if(i==64)
                {
                    Intent s=new Intent(view.getContext(),Dhara66.class);
                    startActivity(s);
                }
                if(i==65)
                {
                    Intent s=new Intent(view.getContext(),Dhara67.class);
                    startActivity(s);
                }
                if(i==66)
                {
                    Intent s=new Intent(view.getContext(),Dhara68.class);
                    startActivity(s);
                }
                if(i==67)
                {
                    Intent s=new Intent(view.getContext(),Dhara69.class);
                    startActivity(s);
                }
                if(i==68)
                {
                    Intent s=new Intent(view.getContext(),Dhara71.class);
                    startActivity(s);
                }
                if(i==69)
                {
                    Intent s=new Intent(view.getContext(),Dhara72.class);
                    startActivity(s);
                }
                if(i==70)
                {
                    Intent s=new Intent(view.getContext(),Dhara73.class);
                    startActivity(s);
                }
                if(i==71)
                {
                    Intent s=new Intent(view.getContext(),Dhara74.class);
                    startActivity(s);
                }
                if(i==72)
                {
                    Intent s=new Intent(view.getContext(),Dhara75.class);
                    startActivity(s);
                }
                if(i==73)
                {
                    Intent s=new Intent(view.getContext(),Dhara76.class);
                    startActivity(s);
                }


            }
        });
        sv.setOnQueryTextListener(this);

    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        String text=newText;
        adapter.getFilter().filter(newText);
        return false;
    }

}

