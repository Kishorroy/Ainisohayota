package com.sculptor.law;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.os.Handler;
import android.content.Intent;
import android.view.Window;

public class Splash extends Activity {
    private boolean backbtnpress;
    private static final int SPLASH_DURATION=5000;
    private Handler myhandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_splash);
        myhandler=new Handler();
        myhandler.postDelayed(new Runnable() {

            @Override
            public void run() {
                // TODO Auto-generated method stub
                finish();
                if(!backbtnpress)
                {
                    Intent intent=new Intent(Splash.this,MainActivity.class);
                    Splash.this.startActivity(intent);
                }

            }
        }, SPLASH_DURATION);



    }


    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        backbtnpress=true;
        super.onBackPressed();
    }
    }

