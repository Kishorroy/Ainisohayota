package com.sculptor.law;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class UserActivity extends AppCompatActivity {
    Button btnemail;
    Button btnback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setLogo(R.drawable.ic_launcher);
        getSupportActionBar().setDisplayUseLogoEnabled(true);
        setContentView(R.layout.activity_user);
        btnemail=(Button)findViewById(R.id.btnemail);
        btnback=(Button)findViewById(R.id.btnback);
        btnback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(UserActivity.this,LoginActivity.class);
                startActivity(i);
                finish();
            }
        });
        final TextView welcomeMsg=(TextView)findViewById(R.id.tvWelcomemsg);
        Intent intent=getIntent();
        String name=intent.getStringExtra("name");
        String message="স্বাগতম " +name;
        welcomeMsg.setText(message);
    }
    public void btnClick(View view)
    {
        Intent i=new Intent(Intent.ACTION_SEND);
        i.setData(Uri.parse("email"));
        String[] s={"scultorit@gmail.com"};
        i.putExtra(Intent.EXTRA_EMAIL,s);
        i.putExtra(Intent.EXTRA_SUBJECT,"সমস্যা এখানে লিখুন");
        i.putExtra(Intent.EXTRA_TEXT," ");
        i.setType("message/rfc822");
        Intent chooser=Intent.createChooser(i,"যোগাযোগ করুন");
        startActivity(chooser);
    }
}
